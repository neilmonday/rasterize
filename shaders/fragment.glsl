#version 450 core

layout (location = 0) out vec4 color;

layout (rgba8) readonly uniform image2D image_data;

void main()
{
    ivec2 position = ivec2(gl_FragCoord.xy);
    color = imageLoad(image_data, position);
}