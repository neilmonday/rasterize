#include <stdio.h>
#include <assert.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "program.h"


typedef struct
{
    GLfloat x, y, z, w;
}vec4;

typedef struct
{
    vec4 p0, p1, p2;
}Triangle;

void window_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

GLfloat dot(vec4 a, vec4 b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w;
}

vec4 subtract(vec4 a, vec4 b)
{
    vec4 result;
    result.x = a.x - b.x;
    result.y = a.y - b.y;
    result.z = a.z - b.z;
    result.w = a.w - b.w;
    return result;
}

vec4 barycentric(Triangle triangle, vec4 point)
{
    //void Barycentric(Point p, Point a, Point b, Point c, float &u, float &v, float &w)
    //{
    vec4 v0 = subtract(triangle.p1, triangle.p0);
    vec4 v1 = subtract(triangle.p2, triangle.p0);
    vec4 v2 = subtract(point, triangle.p0);
    GLfloat d00 = dot(v0, v0);
    GLfloat d01 = dot(v0, v1);
    GLfloat d11 = dot(v1, v1);
    GLfloat d20 = dot(v2, v0);
    GLfloat d21 = dot(v2, v1);
    GLfloat denom = d00 * d11 - d01 * d01;
    vec4 result;
    result.x = (d11 * d20 - d01 * d21) / denom;
    result.y = (d00 * d21 - d01 * d20) / denom;
    result.z = 1.0f - result.x - result.y;
    return result;
}

void main(void)
{
    GLFWwindow* window;
    // Initialise GLFW
    if (!glfwInit())
        return;

    const int width = 1024;
    const int height = 1024;

    // Create a fullscreen mode window and its OpenGL context
    //window = glfwCreateWindow(width, height, "RASTERIZE", glfwGetPrimaryMonitor(), NULL);
    // Create a windowed mode window and its OpenGL context 
    window = glfwCreateWindow(width, height, "RASTERIZE", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return;
    }

    glfwSetWindowSizeCallback(window, &window_size_callback);

    // Make the window's context current
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (GLEW_OK != err)
        return;

    // Initialize 
    GLuint graphics_program = 0;
    GLuint image_location = 1;
    build_graphics_program(&graphics_program, image_location);

    //create a VAO
    GLuint vertex_array_object;
    glGenVertexArrays(1, &vertex_array_object);
    glBindVertexArray(vertex_array_object);

    GLfloat vertices[] = {
        -1.0f, -1.0f,
         1.0f, -1.0f,
        -1.0f,  1.0f,
         1.0f, -1.0f,
         1.0f,  1.0f,
        -1.0f,  1.0f
    };

    //create a VBO
    GLuint vertex_buffer_object;
    glGenBuffers(1, &vertex_buffer_object);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * 4, vertices, GL_STATIC_DRAW);
    err = glGetError(); assert(!err);

    //inputs
    GLint position_location = glGetAttribLocation(graphics_program, "position");
    glVertexAttribPointer(position_location, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(position_location);
    err = glGetError(); assert(!err);

    GLubyte* render_texture_data = calloc(width * height, sizeof(GLubyte) * 4);
    GLuint render_textures[1];
    glCreateTextures(GL_TEXTURE_2D, 1, render_textures);
    glTextureStorage2D(render_textures[0], 1, GL_RGBA8, width, height);
    glTextureParameteri(render_textures[0], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureParameteri(render_textures[0], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    err = glGetError(); assert(!err);

    // Always check that our framebuffer is ok
    GLuint status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
        return;

    glUseProgram(graphics_program);
    glBindImageTexture(image_location, render_textures[0], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);



    Triangle triangle = {
        -0.9f, -0.9f, 0.0f, 1.0f,
         0.9f, -0.9f, 0.0f, 1.0f,
         0.0f,  0.9f, 0.0f, 1.0f
    };

    vec4 point = { 0.0f, 0.0f, 0.0f, 0.0f };
    vec4 test = barycentric(triangle, point);

    //My 3D space will be (-1.0, 1.0) for X and Y and (0.0, 1.0) for Z
    //My screen's space will be (-512, 512) for X and Y and (0.0, 1.0) for Z
/*
    GLfloat maxY, minY;
    GLfloat maxX, minX;



    //inverted slopes. they are inverted (run/rise), because we are scanning horizontally.
    GLfloat inverted_left_slope = (triangle.v2.x - triangle.v0.x) / (triangle.v2.y - triangle.v0.y);
    GLfloat inverted_right_slope = (triangle.v2.x - triangle.v1.x) / (triangle.v2.y - triangle.v1.y);

    //the amount to increment is based on the size of a scanline in screen space compared to that
    //same distance in 3D space.
    GLfloat left_edge_increment = inverted_left_slope / ((float)height / 2.0f);
    GLfloat right_edge_increment = inverted_right_slope / ((float)height / 2.0f);

    int y_int;

    //since v2 is the vertex that is shared by both edges,
    //I am using it as the starting point for the min and max float
    float x_min_float = triangle.v2.x;
    float x_max_float = triangle.v2.x;

    int scale = height / 2;

    for (y_int = -scale; y_int < scale; y_int++)
    {
        float y_float = (float)y_int / (float)scale;
        if ((y_float < minY) || (y_float > maxY))
        {
            continue;
        }

        //I can do the X for loop better by considering these min and max floats.
        x_min_float += right_edge_increment;
        x_max_float += left_edge_increment;

        int x_int;
        for (x_int = -scale; x_int < scale; x_int++)
        {
            float x_float = (float)x_int / (float)scale;
            if ((x_float < minX) || (x_float > maxX))
            {
                continue;
            }

            if ((x_float < x_min_float) || (x_float > x_max_float))
            {
                continue;
            }

            unsigned int index = (((x_int + scale) + ((y_int + scale)*width)) * 4);

            render_texture_data[index + 0] = 0xFF;
            render_texture_data[index + 1] = 0xFF;
            render_texture_data[index + 2] = 0xFF;
            render_texture_data[index + 3] = 0xFF;
        }
    }
    */
    // Loop until the user closes the window 
    while (!glfwWindowShouldClose(window))
    {
        //Update the texture
        glTextureSubImage2D(render_textures[0], 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, render_texture_data);

        //Clear
        const GLfloat color[] = { 0.5f, 0.5f, 0.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, color);

		//Draw the image to the screen
		glDrawArrays(GL_TRIANGLES, 0, 6);

        //SwapBuffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

    glDeleteVertexArrays(1, &vertex_array_object);
    glfwTerminate();
    return;
}
